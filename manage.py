from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

import os
basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

class User(db.Model):
    usn = db.Column(db.String(10), primary_key=True)
    name = db.Column(db.String(50), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    course = db.Column(db.String(50), index=True)
    semester = db.Column(db.Integer, index=True)
    section = db.Column(db.String(1),index=True)
    phone = db.Column(db.String(10),index=True)
    hostelite = db.Column(db.String(3),index=True)
    block = db.Column(db.String(10),index=True)
    room = db.Column(db.String(10),index=True)
    password = db.Column(db.String(128),index=True)
    forgot_password_hash = db.Column(db.String(10),index=True)
    activated = db.Column(db.String(5),index=True)
    activation_code = db.Column(db.String(10),index=True)
    complaints = db.relationship('Complaint', backref='user', lazy='dynamic')

    def __repr__(self):
        return '<User %r>' % (self.usn)

class Administrator(db.Model):
    username = db.Column(db.String(50),primary_key=True)
    name = db.Column(db.String(150),index=True)
    designation = db.Column(db.String(100),index=True)
    password = db.Column(db.String(128),index=True)
    admin_level = db.Column(db.Integer,index=True)
    Complaints = db.relationship('Complaint', backref='administrator', lazy='dynamic')
    forgot_password_hash = db.Column(db.String(10),index=True)
    def __repr__(self):
        return '<Admin %r>' % (self.name)


class Complaint(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(140),index=True)
    creationdate = db.Column(db.DateTime,index=True)
    comments = db.Column(db.String(100), index=True)
    status = db.Column(db.String(10), index=True)
    # subject = db.Column(db.String(25), index=True)
    resolvedate = db.Column(db.DateTime,index=True)
    type = db.Column(db.Integer,index=True)
    user_usn = db.Column(db.String(10),db.ForeignKey('user.usn'))
    resolver_name = db.Column(db.String(50),db.ForeignKey('administrator.username') )

    def __repr__(self):
        return '<Complaint %r>' % (self.body)

if __name__ == '__main__':
    manager.run()
