# Hostel/Campus Complaint System



### Aim

Using the portal students of a praticular college (Ex: BMSIT&M)  will be able to login and book complaints about campus and hostel. These complaints will be recorded on the server and displayed to administrators when they login. 
 
Students are of two catergories

 * Hostelites - Can book complaints about hostel facilities and campus facilities
 * Non-Hostelities (Day Scholar) - Can book complaints related to campus only

Administrators are of three catergories

* Super administrator - Can view and take action on complaints related to hostel and campus 
* Hostel administrator - Can view and take action on complaints related to hostel only
* Campus  administrator - Can view and take action on complaints related to campus only


### Requirement analysis

* Usually around 50-60 complaints will be booked on daily basis
* The system must be able to handle around 10-15 simultaneous logins
* Maximum of  10 complaints  will be resolved in a day's time
* The server usage time will be around 4-5 hours per day
* Peak usage timings: 12pm to 2pm and 6pm to 8pm
* The system must be able to store all the complaints securly and retirve at most 10 complaints in a single request initially
* The system must be designed to handle more requests in the future

### Design and Implementation

#### Implentation using IaaS

* Front-end : HTML5,CSS3 and JS (Uses Bootstrap plugin)
* Hosted on : [DigitalOcean](https://www.digitalocean.com/) (VPS Cloud Hosting Service) using 5$ plan initially
	* 512 MB memory
    * 1 core processor
    *  20 GB SSD disk
    *  1 TB hard disk
    *  Operating system: Ubuntu 16.04 server
* Middleware : Python Flask Framework (running on the VPS server)
* Database: SQL Alchemy Database - stored in Digital ocean droplet as app.db

* Other prcining availble at [DigitalOceanPricing](https://www.digitalocean.com/pricing/)
*  Github URL: https://gitlab.com/gsraman/hostel-complaint

### Quick links and details to login


#### Student registration

*  Fill up details using correct USN other details 
*  Check email for verification email
*  Login after Veriication and lodge complaints
*  View status of complaints

#### Admin login details

* Email: principal@mailinator.com
* Password: hello123








