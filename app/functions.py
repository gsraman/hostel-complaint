from flask import render_template,request,redirect,url_for,session
from app import app,models,db
import hashlib
from sqlalchemy import exc
import sqlite3
import datetime
import random
import string
from flask.ext.pystmark import Pystmark, Message
from pystmark import ResponseError


def random_string(length):
    pool = string.letters + string.digits
    return ''.join(random.choice(pool) for i in xrange(length))

def send_activation_link(usn,email):
    try:
        pystmark = Pystmark(app)
        u = models.User.query.filter_by(usn=usn,email=email,activated="false").first()
        activation_code  = random_string(10)
        print str(activation_code)
        u.activation_code = str(activation_code)
        db.session.commit()
        message = 'Hello '+u.name+',<br><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please click on following link to activate your account  <a href="http://139.59.5.22/activate/'+str(usn)+'/'+str(activation_code)+'">http://139.59.5.22/activate/'+str(usn)+'/'+str(activation_code)+'</a><br><br>'
        m = Message(to=u.email, subject='Account activation Link', html=message)
        resp = pystmark.send(m)
        print "Activation email sent to "+str(u.email)
        try:
            resp.raise_for_status()
        except ResponseError as e:
            return 'Failed to send message. Reason: {}'.format(e)
        else:
            #return 'Sent message to {}'.format(resp.message.to)
            return render_template('register.html',message="Activation link sent to your registered email address",heading="Please check email",status='green')
    except:
        return render_template('register.html',message="Email or USN doesn't match.Contact admin of problem persists",heading="Login to continue",status='red')

@app.route('/register',methods=['POST'])
def register():
    usn = request.form['student_usn']
    name = request.form['student_name']
    course = request.form['student_course']
    semester = request.form['student_semester']
    section = request.form['student_section']
    email = request.form['student_email']
    phone = request.form['student_phone']
    hostelite = request.form['student_bool_hostel']
    block = request.form['student_hostel_block']
    room = request.form['student_room_number']
    password = request.form['student_password_1']
    password1 = request.form['student_password_2']
    activation_code = random_string(10)
    h = hashlib.sha256()
    h.update('7gyhu'+password+'0jgkj')
    new_password = h.hexdigest()
    if '1BY' not in usn and len(usn.strip(' ')) != 10:
        return render_template('register.html',message='Please enter valid USN',heading="Couldn't register User!",status='red')
    elif len(password)<8:
        return render_template('register.html',message='Please enter longer password',heading="Couldn't register User!",status='red')
    elif password != password1:
        return render_template('register.html',message="Passwords don't match",heading="Couldn't register User!",status='red')
    else:
        try:
            if hostelite == "yes":
                u = models.User(usn=usn,name=name,course=course,semester=semester,section=section,email=email,phone=phone,hostelite=hostelite,block=block,room=room,password=new_password,activated="false",activation_code=activation_code)
            else :
                u = models.User(usn=usn,name=name,course=course,semester=semester,section=section,email=email,phone=phone,hostelite=hostelite,password=new_password,activated="false",activation_code=activation_code)
            db.session.add(u)
            db.session.commit()
            send_activation_link(u.usn,u.email)
            return render_template('register.html',message="Successfully Registered. Check email for activation link.",heading="User Sccessfully Registered!",status='green')
        except exc.IntegrityError as e:
            return render_template('register.html',message="USN,Email or phone number already exists",heading="Couldn't register User!",status='red')

@app.route('/student/login',methods=['POST'])
def login():
    usn = request.form['student_usn']
    password = request.form['student_password']
    try:
        remember = request.form['remember']
    except:
        remember = 'no'
    h = hashlib.sha256()
    h.update('7gyhu'+password+'0jgkj')
    new_password = h.hexdigest()
    try:
        user = models.User.query.filter_by(usn=usn).first()
        if new_password == user.password:
            if user.activated != "false":
                session['usn'] = str(user.usn)
                if remember == 'no':
                    session.permanent = False
                elif remember == 'remember':
                    session.permanent = True
                return redirect('/student/dashboard')
            else:
                return render_template('register.html',message="User not activated. Please check your mail for registration link or visit activation link. ",heading="User not activated",status='red')
        else:
            return render_template('register.html',message="USN or password didn't match!   ",heading="Coudn't login User",status='red')
    except AttributeError as e:
        return render_template('register.html',message="User not found. Please register!",heading="Coudn't login User",status='red')

@app.route('/register-complaint',methods=['POST'])
def register_complaint():
    complaint_type = request.form['complaint_type']
    # complaint_subject = request.form['complaint_subject']
    complaint_details = request.form['complaint_details']
    complaint_creationdate_intermediate = datetime.date.today()
    complaint_creationdate = str(complaint_creationdate_intermediate.strftime('%d-%m-%y'))
    complaint_status = "pending"
    c = models.Complaint(body=complaint_details,creationdate=complaint_creationdate,status=complaint_status,type=complaint_type,user_usn=session['usn'])
    try:
        db.session.add(c)
        db.session.commit()
        return redirect('/student/list-complaints')
    except:
        return "Complaint couldn't be registered! Try again"




@app.route('/student/logout')
def logout():
    session.pop('usn',None)
    return redirect(url_for('index'))

@app.route('/register-admin',methods=['POST'])
def register_admin():
    username = request.form['admin_username']
    name = request.form['admin_name']
    designation = request.form['admin_designation']
    password = request.form['admin_password']
    admin_level = request.form['admin_level']
    master_password = request.form['master_password']
    h = hashlib.sha256()
    h.update('7gyhu'+password+'0jgkj')
    new_password = h.hexdigest()
    g = hashlib.sha256()
    g.update('7gyhu'+master_password+'0jgkj')
    master_password1 = g.hexdigest()
    if master_password1 == '57f3e9ce8b41a2bb852b487ccc56bd9967e1787ffbbd18a74f9609d9ae7e10e5':
        try:
            a = models.Administrator(username=username,name=name,designation=designation,password=new_password,admin_level=admin_level)
            db.session.add(a)
            db.session.commit()
            return render_template('register.html',message="Admin Successfully Registered",heading="Admin Sccessfully Registered!",status='green')
        except exc.IntegrityError as e:
            return render_template('register.html',message="Admin not registered",heading="Couldn't register admin!",status='red')

@app.route('/admin/login',methods=['POST'])
def admin_login():
        username = request.form['admin_username']
        password = request.form['admin_password']
        try:
            remember = request.form['remember']
        except:
            remember = 'no'
        h = hashlib.sha256()
        h.update('7gyhu'+password+'0jgkj')
        new_password = h.hexdigest()
        try:
            admin = models.Administrator.query.filter_by(username=username).first()
            if new_password == admin.password:
                session['username'] = str(admin.username)
                if remember == 'no':
                    session.permanent = False
                elif remember == 'remember':
                    session.permanent = True
                return redirect('/admin/dashboard')
            else:
                return render_template('register.html',message="Email or password didn't match   ",heading="Coudn't login Admin",status='red')
        except AttributeError as e:
            return render_template('register.html',message="User not found. Please register!",heading="Coudn't login Admin",status='red')

@app.route('/admin/logout',methods=['GET','POST'])
def admin_logout():
        if 'username' in session:
            session.pop('username',None)
            return redirect(url_for('index'))

@app.route('/action/<int:complaint_id>',methods=['GET','POST'])
def action(complaint_id):
     if 'username' in session:
        admin = models.Administrator.query.filter_by(username=session['username']).first()
        complaint = models.Complaint.query.filter_by(id=complaint_id).first()
        user = models.User.query.filter_by(usn=complaint.user_usn).first()
        # return str(data['admin_level']) + c.type
        if str(admin.admin_level) == "2" and complaint.type != "campus" :
            return "You do not have enough permission to take action on this complaint"
        elif str(admin.admin_level) == "3" and complaint.type != "hostel" :
            return "You do not have enough permission to take action on this complaint"
        else :
            return render_template('administrator/complaint_action.html',admin=admin,complaint=complaint,user=user)

     else:
        return render_template('register.html',message="Please login",heading="Login to continue",status='red')

@app.route('/complaint-action',methods=['POST'])
def complaint_action():
    complaint_id = request.form['complaint_id']
    complaint_comment = request.form['complaint_comment']
    complaint_status = request.form['complaint_status']
    complaint_resolver = request.form['complaint_resolver']
    complaint_resolvedate_intermediate = datetime.date.today()
    complaint_resolvedate = str(complaint_resolvedate_intermediate.strftime('%d-%m-%y'))
    try:
        c =  models.Complaint.query.filter_by(id=complaint_id).first()
        r =  models.Administrator.query.filter_by(username=complaint_resolver).first()
        c.resolver_name = r.username;
        c.comments = complaint_comment;
        c.status = complaint_status;
        c.resolvedate = complaint_resolvedate;
        db.session.commit()
        if c.status == "resolved":
            return redirect('/complaints/resolved')
        elif c.status == "pending":
            return redirect('/complaints/pending')
        elif c.status == "denied":
            return redirect('/complaints/denied')
        else:
            return "Unknown status!"
    except:
        return "Please try again!"

    return str(complaint_id)+' '+str(complaint_comment)+' '+str(complaint_status)+' '+str(complaint_resolver)+' '+str(complaint_resolvedate)

@app.route('/student-reset-password',methods=['POST'])
def student_reset_password():
    usn = request.form['student_usn']
    email = request.form['student_email']
    try:
        pystmark = Pystmark(app)
        u = models.User.query.filter_by(usn=usn,email=email).first()
        forgot_password_hash  = random_string(10)
        print str(forgot_password_hash)
        u.forgot_password_hash = str(forgot_password_hash)
        db.session.commit()
        message = 'Hello '+u.name+',<br><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your password reset link for your account is  <a href="http://139.59.5.22/reset/'+str(forgot_password_hash)+'">http://139.59.5.22/reset/'+str(forgot_password_hash)+'<a><br><br>Ignore if you did not request for password change'
        m = Message(to=u.email, subject='Password Reset Link', html=message)
        resp = pystmark.send(m)
        print "Retrival email sent to "+str(u.email)
        try:
            resp.raise_for_status()
        except ResponseError as e:
            return 'Failed to send message. Reason: {}'.format(e)
        else:
            #return 'Sent message to {}'.format(resp.message.to)
            return render_template('register.html',message="Reset password link sent to your registered email address",heading="Please check email",status='green')
    except:
        return render_template('register.html',message="Email or USN doesn't match.Contact admin of problem persists",heading="Login to continue",status='red')

@app.route('/student-confirm-password',methods=['POST'])
def student_confirm_password():
    usn = request.form['student_usn']
    password = request.form['student_password']
    password1 = request.form['student_password1']
    try:
        u = models.User.query.filter_by(usn=usn).first()
        if password == password1 and len(password) >= 8:
            h = hashlib.sha256()
            h.update('7gyhu'+password+'0jgkj')
            new_password = h.hexdigest()
            u.forgot_password_hash = ''
            u.password = new_password
            db.session.commit()
            return render_template('register.html',message="Password reset Successfully.You may now login",heading="Password reset successful",status='green')
        else:
            return render_template('register.html',message="Please visit the unique link again. Passwords need to be atleast 8 digits long",heading="Passwords didn't match ",status='red')
    except:
        return render_template('register.html',message="Please try again or contact administrator for help",heading="Unknown Error occured",status='red')

@app.route('/admin-reset-password',methods=['POST'])
def admin_reset_password():
    email = request.form['admin_email']
    try:
        pystmark = Pystmark(app)
        u = models.Administrator.query.filter_by(username=email).first()
        forgot_password_hash  = random_string(10)
        print str(forgot_password_hash)
        u.forgot_password_hash = str(forgot_password_hash)
        db.session.commit()
        message = 'Hello '+u.name+',<br><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your password reset link for your account is  <a href="http://139.59.5.22/reset/admin/'+str(forgot_password_hash)+'">http://139.59.5.22/reset/admin/'+str(forgot_password_hash)+'<a><br><br>Ignore if you did not request for password change'
        m = Message(to=u.username, subject='Password Reset Link', html=message)
        resp = pystmark.send(m)
        print "Retrival email sent to "+str(u.username)
        try:
            resp.raise_for_status()
        except ResponseError as e:
            return 'Failed to send message. Reason: {}'.format(e)
        else:
            #return 'Sent message to {}'.format(resp.message.to)
            return render_template('register.html',message="Reset password link sent to your registered email address",heading="Please check email",status='green')
    except:
        return render_template('register.html',message="Email or USN doesn't match.Contact admin of problem persists",heading="Login to continue",status='red')

@app.route('/admin-confirm-password',methods=['POST'])
def admin_confirm_password():
    username = request.form['admin_username']
    password = request.form['admin_password']
    password1 = request.form['admin_password1']
    try:
        u = models.Administrator.query.filter_by(username=username).first()
        if password == password1 and len(password) >= 8:
            h = hashlib.sha256()
            h.update('7gyhu'+password+'0jgkj')
            new_password = h.hexdigest()
            u.forgot_password_hash = ''
            u.password = new_password
            db.session.commit()
            return render_template('register.html',message="Password reset Successfully.You may now login",heading="Password reset successful",status='green')
        else:
            return render_template('register.html',message="Please visit the unique link again. Passwords need to be atleast 8 digits long",heading="Passwords didn't match ",status='red')
    except:
        return render_template('register.html',message="Please try again or contact administrator for help",heading="Unknown Error occured",status='red')

@app.route('/resend-activation-link',methods=['POST'])
def resend_activation_link():
    usn = request.form['student_usn']
    email = request.form['student_email']
    try:
        pystmark = Pystmark(app)
        u = models.User.query.filter_by(usn=usn,email=email,activated="false").first()
        activation_code  = random_string(10)
        print str(activation_code)
        u.activation_code = str(activation_code)
        db.session.commit()
        message = 'Hello '+u.name+',<br><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please click on following link to activate your account  <a href="http://139.59.5.22/activate/'+str(usn)+'/'+str(activation_code)+'">http://139.59.5.22/activate/'+str(usn)+'/'+str(activation_code)+'</a><br><br>'
        m = Message(to=u.email, subject='Account activation Link', html=message)
        resp = pystmark.send(m)
        print "Activation email sent to "+str(u.email)
        try:
            resp.raise_for_status()
        except ResponseError as e:
            return 'Failed to send message. Reason: {}'.format(e)
        else:
            #return 'Sent message to {}'.format(resp.message.to)
            return render_template('register.html',message="Activation link sent to your registered email address",heading="Please check email",status='green')
    except:
        return render_template('register.html',message="Email or USN doesn't match.Contact admin of problem persists",heading="Login to continue",status='red')
