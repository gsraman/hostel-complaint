from flask import render_template,request,redirect,url_for,session
from app import app


@app.errorhandler(400)
def method_not_found(error):
    return redirect(url_for('index'))
