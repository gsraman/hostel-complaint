from flask import render_template,request,redirect,url_for,session
from flask_paginate import Pagination
from app import app,models,db
from sqlalchemy import desc

@app.route('/')
@app.route('/index')
def index():
    if 'usn' in session:
        return redirect('/student/dashboard')
    elif 'username' in session:
        return redirect('/admin/dashboard')
    else:
        return render_template('register.html')

@app.route('/student/dashboard')
def dashboard():
    if 'usn' in session:
        user = models.User.query.filter_by(usn=session['usn']).first()
        return render_template('student/dashboard.html',user=user)
    else:
        return redirect(url_for('index'))


@app.route('/student/profile')
def profile():
    if 'usn' in session:
        user = models.User.query.filter_by(usn=session['usn']).first()
        return render_template('student/student_profile.html',user=user)
    else:
        return redirect(url_for('index'))

@app.route('/student/complaint')
def complaint():
    if 'usn' in session:
        user = models.User.query.filter_by(usn=session['usn']).first()
        return render_template('student/new_complaint.html',user=user)
    else:
        return redirect(url_for('index'))

@app.route('/student/list-complaints')
@app.route('/student/list-complaints/<int:page>')
def list_complaint(page=1):
    if 'usn' in session:
        # search = True
        # page = request.args.get('page', type=int, default=1)
        user = models.User.query.filter_by(usn=session['usn']).first()
        complaints = models.Complaint.query.filter_by(user_usn=session['usn']).order_by(desc(models.Complaint.id)).paginate(page, 10, False)
        # complaints.reverse()
        return render_template('student/view_complaints.html',user=user,complaints=complaints)
    else:
        return redirect(url_for('index'))

@app.route('/complaint/<int:complaint_id>')
def id_complaint(complaint_id):
    if 'usn' in session:
        user = models.User.query.filter_by(usn=session['usn']).first()
        complaint = models.Complaint.query.filter_by(user_usn=session['usn'],id=complaint_id).first()
        resolver = models.Administrator.query.filter_by(username=complaint.resolver_name).first()
        try:
            return render_template('student/complaint_details.html',complaint=complaint,user=user,resolver=resolver)
        except:
            return "You don't have sufficient access permissions to view this complaint"

    elif 'username' in session:
        admin = models.Administrator.query.filter_by(username=session['username']).first()
        complaint = models.Complaint.query.filter_by(id=complaint_id).first()
        resolver = models.Administrator.query.filter_by(username=complaint.resolver_name).first()
        user = models.User.query.filter_by(usn=complaint.user_usn).first()
        try:
            if admin.admin_level==2 and complaint.type=="campus":
                return render_template('administrator/complaint_details.html',admin=admin,complaint=complaint,user=user,resolver=resolver)
            elif admin.admin_level==3 and complaint.type=="hostel":
                return render_template('administrator/complaint_details.html',admin=admin,complaint=complaint,user=user,resolver=resolver)
            elif admin.admin_level==1:
                return render_template('administrator/complaint_details.html',admin=admin,complaint=complaint,user=user,resolver=resolver)
            else:
                return "You don't have access to this complaint!"

        except:
            return "You have no permission for accessing the complaint"

    else:
        return redirect(url_for('index'))

@app.route('/admin/register')
def admin_register():
    return render_template('create_admin.html')

@app.route('/admin/dashboard')
def admin_dashboard():
    if 'username' in session:
        admin = models.Administrator.query.filter_by(username=session['username']).first()
        return render_template('administrator/dashboard.html',admin=admin)
    else:
        return render_template('register.html',message="Please login",heading="Login to continue",status='red')

@app.route('/admin/profile')
def admin_profile():
    if 'username' in session:
        admin = models.Administrator.query.filter_by(username=session['username']).first()
        return render_template('administrator/admin_profile.html',admin=admin)
    else:
        return render_template('register.html',message="Please login",heading="Login to continue",status='red')

@app.route('/complaints/<status>')
@app.route('/complaints/<status>/<int:page>')
def complaints_pending(status,page=1):
    if 'username' in session:
        status = status
        admin = models.Administrator.query.filter_by(username=session['username']).first()
        if admin.admin_level == 1:
            complaints = models.Complaint.query.filter_by(status=status).order_by(desc(models.Complaint.id)).paginate(page, 10, False)
        elif admin.admin_level == 2:
            complaints = models.Complaint.query.filter_by(status=status,type="campus").order_by(desc(models.Complaint.id)).paginate(page, 10, False)
        elif admin.admin_level == 3:
            complaints = models.Complaint.query.filter_by(status=status,type="hostel").order_by(desc(models.Complaint.id)).paginate(page, 10, False)

        return render_template('administrator/view_complaints.html',admin=admin,complaints=complaints,status=status)

    else:
        return render_template('register.html',message="Please login",heading="Login to continue",status='red')

@app.route('/student/forgot-password')
def forgot_student_password():
    return render_template('student/forgot-password.html')

@app.route('/admin/forgot-password')
def forgot_admin_password():
    return render_template('administrator/forgot-password.html')

@app.route('/reset/<reset_id>')
def reset_password(reset_id):
    try:
        u = models.User.query.filter_by(forgot_password_hash=reset_id).first()
        print u.usn
        return render_template('student/reset-password.html',reset_id=reset_id,usn=u.usn)
    except:
        return render_template('register.html',message="Please try again or contact administrator for help",heading="Unknown Error occured",status='red')
# set the secret key.  keep this really secret:

@app.route('/reset/admin/<reset_id>')
def reset_admin_password(reset_id):
    try:
        u = models.Administrator.query.filter_by(forgot_password_hash=reset_id).first()
        print u.username
        return render_template('administrator/reset-password.html',reset_id=reset_id,username=u.username)
    except:
        return render_template('register.html',message="Please try again or contact administrator for help",heading="Unknown Error occured",status='red')

@app.route('/activate')
def activate_url():
    return render_template('student/activate_account.html')

@app.route('/activate/<usn>/<activation_code>')
def activate_account(usn,activation_code):
    try:
        u = models.User.query.filter_by(usn=usn,activation_code=activation_code).first()
        # print u.username
        u.activated = "true"
        u.activation_code = ''
        db.session.commit()
        return render_template('register.html',message='Account activated. You may now login',heading='Account activation successful',status='green')
    except:
        return render_template('register.html',message='Account not activated. try again',heading='Account activation unsuccessful',status='red')
