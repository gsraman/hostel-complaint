"""empty message

Revision ID: 15683f587d62
Revises: 2a239477e555
Create Date: 2016-09-04 20:55:07.628452

"""

# revision identifiers, used by Alembic.
revision = '15683f587d62'
down_revision = '2a239477e555'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('administrator', sa.Column('forgot_password_hash', sa.String(length=10), nullable=True))
    op.create_index(op.f('ix_administrator_forgot_password_hash'), 'administrator', ['forgot_password_hash'], unique=False)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_administrator_forgot_password_hash'), table_name='administrator')
    op.drop_column('administrator', 'forgot_password_hash')
    ### end Alembic commands ###
