import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SECRET_KEY = 'A0rr94j/3YX R~XHH!jmN]LWX/,?RT'
PYSTMARK_API_KEY = '77f4a679-7349-4fe5-a772-cefd527dd1bf'
PYSTMARK_DEFAULT_SENDER ='sundar@bmsit.in'
